# ezNmon-Manager

#### 项目原始地址：https://gitee.com/goodhal/ezNmon-Manager

#### 对原始项目做了一些优化，下载地址：https://gitee.com/zhaoyafan/ezNmon-Manager/releases

#### ezNmon-Manager是[easyNmon](https://gitee.com/mzky/easyNmon)的Web版多机部署工具管理控制台，使用Spring Boot以jar形式发布，自带sqlite数据库，使用`java -jar eznmonmgr.jar`即可启动使用，默认端口8888；

#### 可远程批量操作多台服务器的监控服务部署和基本性能实时监控，支持Excel批量导入主机，自动生成基于nmon的HTML和nmon报告原始数据文件下载，目标服务器在不需要安装任何语言环境和插件的情况下进行Linux系统资源监控；

#### 对内置easyNmon进行修改重新编译：

##### 1.优化生成Report目录命名规则；

##### ![image-20230222142559326](https://www.fanscloud.net/typoraUpload/images/20230222/8fbb2cd20640d694.png)

##### 2.优化Report模板文件，实现报告打包后能够本地查看或直接在压缩包中查看；

##### 3.启动easyNmon前设置TZ=GMT-8(TimeZome)环境变量，解决HTML报告时区问题；

##### ![image-20230222142443951](https://www.fanscloud.net/typoraUpload/images/20230222/86ea31ea7b7463b0.png)

#### JAVA代码中：

##### 1.优化安装、卸载服务、下载报告、清空报告等功能执行的Linux命令；

##### 2.Web控制台界面优化；

##### ![image-20230222142228277](https://www.fanscloud.net/typoraUpload/images/20230222/7daaa2863c5e9292.png)

##### ![image-20230222143010643](https://www.fanscloud.net/typoraUpload/images/20230222/19f19dd678b7e904.png)

#### 下载源码编译打包该项目需要使用JDK-8u212版本；

