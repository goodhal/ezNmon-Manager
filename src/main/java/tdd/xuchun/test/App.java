package tdd.xuchun.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tdd.xuchun.test.util.TarUntil;

import java.io.IOException;

/**
 * @SpringBootApplication指定这是一个 spring boot的应用程序.
 */
@SpringBootApplication
public class App  {
	
    public static void main( String[] args ) throws IOException {
    	// SpringApplication 用于从main方法启动Spring应用的类。
        TarUntil.getdiskFile("/nmon/easyNmon.tar.gz");
        TarUntil.getdiskFile("/nmon/nmonanaly.xlsm");
        SpringApplication.run(App.class, args);
    }
}
