package tdd.xuchun.test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tdd.xuchun.test.service.IEasyNmonService;
import tdd.xuchun.test.model.Host;
import tdd.xuchun.test.model.Result;
import tdd.xuchun.test.service.IHostService;

/**
 * 主机控制器
 * 
 * @author ZSL
 *
 */
@RestController
@RequestMapping("easynmon")
public class EasyNmonController {

	/**
	 * 注入主机业务服务
	 */
	@Autowired
	private IHostService hostService;

	@Autowired
	private IEasyNmonService easyNmonService;

	@RequestMapping("/setupandrun")
	public Result setupAndRun(int id) throws InterruptedException {
		String message = "";
		Host host = new Host();
		host.setId(id);
		host = hostService.getHost(host);
		int code = easyNmonService.accessTheURL(host, "");
		if (code != 200) {
			easyNmonService.upAndRun(host, 0);
			Thread.sleep(2000);
			code = easyNmonService.accessTheURL(host, "");
		}
		if (code == 200) {
			message += host.getName() + "[" + host.getAddr() + "]" + "安装成功\n";
		} else {
			message += host.getName() + "[" + host.getAddr() + "]" + "安装失败\n";
		}
		Result rs = new Result();
		rs.setMessage(message);
		return rs;
	}

	/**
	 * 覆盖安装服务
	 * 
	 * @return
	 */
	@RequestMapping("/setupandruncover")
	public Result setupAndRunCover(int id) throws InterruptedException{
		String message = "";
		Host host = new Host();
		host.setId(id);
		host = hostService.getHost(host);
		boolean flag = easyNmonService.upAndRun(host, 1);
		Thread.sleep(2000);
		int code = 0;
		if (flag) {
			code = easyNmonService.accessTheURL(host, "");
		}
		if (code == 200) {
			message = host.getName() + "[" + host.getAddr() + "]" + "安装成功\n";
		} else {
			message = host.getName() + "[" + host.getAddr() + "]" + "安装失败\n";
		}
		Result rs = new Result();
		rs.setMessage(message);
		return rs;
	}

	@RequestMapping("/startmoniter")
	public Result startMoniter(int id, @RequestParam(name = "t") String t, @RequestParam(name = "f") Integer f) {
		String message = "";
		Host host = new Host();
		host.setId(id);
		host = hostService.getHost(host);
		int code = easyNmonService.accessTheURL(host, "start?n=" + host.getName() + "&t=" + t + "&f=" + f);
		if (code == 200) {
			message = host.getName() + "[" + host.getAddr() + "]" + "监控开启成功\n";
		} else {
			message = host.getName() + "[" + host.getAddr() + "]" + "监控开启失败[" + code + "]\n";
		}
		Result rs = new Result();
		rs.setMessage(message);
		return rs;
	}

	@RequestMapping("/stopmoniter")
	public Result stopMoniter(int id) {
		String message = "";
		Host host = new Host();
		host.setId(id);
		host = hostService.getHost(host);
		int code = easyNmonService.accessTheURL(host, "stop");
		if (code == 200) {
			message = host.getName() + "[" + host.getAddr() + "]" + "监控停止成功\n";
		} else {
			message = host.getName() + "[" + host.getAddr() + "]" + "监控停止失败[" + code + "]\n";
		}
		Result rs = new Result();
		rs.setMessage(message);
		return rs;
	}

	@RequestMapping("/clearreport")
	public Result clearReport(int id) {
		String message = "";
		Host host = new Host();
		host.setId(id);
		host = hostService.getHost(host);
		int statecode = easyNmonService.accessTheURL(host, "stop");
		if (statecode == 200) {
			message = host.getName() + "[" + host.getAddr() + "]" + "监控停止成功\n";
		} else {
			message = host.getName() + "[" + host.getAddr() + "]" + "监控停止失败[" + statecode + "]\n";
		}
		boolean code = easyNmonService.clearReport(host);
		if (code) {
			message = host.getName() + "[" + host.getAddr() + "]" + "报告删除成功\n";
		} else {
			message = host.getName() + "[" + host.getAddr() + "]" + "报告删除失败\n";
		}

		Result rs = new Result();
		rs.setMessage(message);
		return rs;
	}

	@RequestMapping("/getreport")
	public Result getReport(int id) {
		String message = "";
		Host host = new Host();
		host.setId(id);
		host = hostService.getHost(host);
		boolean code = easyNmonService.getReportPackage(host);
		if (code) {
			message = "http://" + host.getAddr() + ":" + host.getEznmonport() + "/report/easyNmonReport_" + host.getName() + ".zip";
		} else {
			message = "打包报告出错";
		}

		Result rs = new Result();
		rs.setMessage(message);
		return rs;
	}

	@RequestMapping("/geteasynmonstate")
	public Result getEasyNmonState(int id) {
		String message = "";
		Host host = new Host();
		host.setId(id);
		host = hostService.getHost(host);
		int code = easyNmonService.accessTheURL(host, "");
		message += id + "_" + code + "_" + host.getName();
		Result rs = new Result();
		rs.setMessage(message);
		return rs;
	}

	/*
	 * 获得easyNmon支持的操作系统类型
	 */
	@RequestMapping("/getostype")
	public Result getEasyNmonState() {
		String message = "";
		List<String> osarray = easyNmonService.getEzNmonOS();
		message = osarray.toString().replace("[", "").replace("]", "");
		Result rs = new Result();
		rs.setMessage(message);
		return rs;
	}
	/**
	 * 清空主机上安装的nmon服务
	 */

	@RequestMapping("/clearservice")
	public Result clearService(int id){
		Host host = new Host();
		host.setId(id);
		host = hostService.getHost(host);
		boolean flag=easyNmonService.clearService(host);
		Result rs = new Result();
		String message="";
		if(flag){
			message = host.getName() + "[" + host.getAddr() + "]" + "卸载成功\n";
		}else{
			message = host.getName() + "[" + host.getAddr() + "]" + "卸载失败\n";
		}
		rs.setMessage(message);
		return rs;
	}

}
