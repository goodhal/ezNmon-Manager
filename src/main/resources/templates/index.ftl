﻿<!DOCTYPE html>
<html lang="zh">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>easyNmon管理平台</title>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/layui.css" media="all">
    <script src="${request.contextPath}/js/vue/vue.min.js"></script>
    <script src="${request.contextPath}/js/jquery/jquery.min.js"></script>
    <script src="${request.contextPath}/js/laypage/laypage.js" charset="utf-8"></script>
    <script src="${request.contextPath}/js/layer/layer.js" charset="utf-8"></script>
    <script src="${request.contextPath}/js/bootstrap/bootstrap.min.js"></script>
    <script src="${request.contextPath}/js/layui/layui.js"></script>
    
    <body>
        <div id="app" class="container">
            <div class="page-header">
                <h3>easyNmon管理平台</h3></div>
            <div class="panel-body">
                <form class="form-inline bg-info" role="form">查找条件：
                    <div class="form-group">
                        <label class="sr-only" for="name">主机名称</label>
                        <input type="text" class="form-control" id="name" placeholder="主机名称">
                        <label class="sr-only" for="addr">IP</label>
                        <input type="text" class="form-control" id="addr" placeholder="IP">
                        <label class="sr-only" for="remark">备注</label>
                        <input type="text" class="form-control" id="remark" placeholder="备注" style="width:235px"></div>
                    <div class="btn-group">
                        <button type="button" id="findHost" class="btn btn-success">
                            <span class="glyphicon glyphicon-search"></span>查找主机</button>
                        <button type="button" id="addHostBtn" class="btn btn-primary">
                            <span class="glyphicon glyphicon-plus"></span>新增主机</button>
                        <button type="button" id="importHostBtn" class="btn btn-danger">
                            <span class="glyphicon glyphicon-upload"></span>批量导入</button>
                        <button type="button" id="downloadtempleBtn" class="btn btn-info" onclick="downloadTemplate()">
                            <span class="glyphicon glyphicon-download"></span>下载模板</button>
                    </div>
                </form>
                <br>
                <form class="form-inline bg-warning" role="form">服务管理：
                    <div class="btn-group">
                        <button type="button" id="setupandRun" class="btn btn-primary" onclick="setUpServer()">
                            <span class="glyphicon glyphicon-send"></span>安装服务</button>
                        <button type="button" onclick="clearservice()" class="btn btn-danger">
                            <span class="glyphicon glyphicon-trash"></span>卸载服务</button>
                    </div>监控频率：
                    <div class="form-group">
                        <label class="sr-only" for="name">监控频率(s)</label>
                        <input type="text" class="form-control" id="f" value="5" placeholder="监控频率(s)"></div>监控时长：
                    <div class="form-group">
                        <label class="sr-only" for="addr">时长(分钟)</label>
                        <input type="text" class="form-control" id="t" value="30" placeholder="时长(分钟)"></div>
                    <div class="btn-group">
                        <button type="button" id="startMoniter" onclick="startEasyNmon()" class="btn btn-success">
                            <span class="glyphicon glyphicon-play"></span>开始监控</button>
                        <button type="button" id="stopMoniter" onclick="stopEasyNmon()" class="btn btn-warning">
                            <span class="glyphicon glyphicon-stop"></span>停止监控</button>
                        <button type="button" onclick="clearReport()" class="btn btn-danger">
                            <span class="glyphicon glyphicon-remove-circle"></span>清空报告</button>
                    </div>
                </form>
            </div>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr class="success">
                            <td>
                                <input type="checkbox" name="ids" id="ids" onchange="chgAll(this)" /></td>
                            <!--<td><b>序号</b></td>-->
                            <td>
                                <b>主机名称</b>
                            </td>
                            <td>
                                <b>IP</b>
                            </td>
                            <td>
                                <b>备注</b>
                            </td>
                            <td>
                                <b>监控端口</b>
                            </td>
                            <td>
                                <b>服务状态</b>
                            </td>
                            <td>
                                <b>操作</b>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="active" v-for="(item,index) in result">
                            <td>
                                <input type="checkbox" name="id" :value="item.id" onchange="chg(this)"></td>
                            <!--<td>{{index+1}}</td>-->
                            <td :id="'name_'+item.id">
                                <a data-toggle="tooltip" title="查看报告" @click="showConsole(item.addr,item.eznmonport)">{{item.name}}</a></td>
                            <td>{{item.addr}}</td>
                            <td>{{item.remark}}</td>
                            <td>{{item.eznmonport}}</td>
                            <td :id="'stat_'+item.id"></td>
                            <td>
                                <a data-toggle="tooltip" title="编辑主机" @click="editEvent(item.id)">编辑</a>
                                <a data-toggle="tooltip" title="删除主机" @click="delEvent(item.id)">删除</a>
                                <a data-toggle="tooltip" title="在线查看报告" @click="showConsole(item.addr,item.eznmonport)">查看报告</a>
                                <a data-toggle="tooltip" title="打包下载报告" @click="getReport(item.id)">下载报告</a></td>
                        </tr>
                    </tbody>
                </table>
                <div id="pagenav"></div>
                <a href="https://gitee.com/goodhal/ezNmon-Manager" data-toggle="tooltip" target="_blank">更多信息</a></div>
        </div>
        <script>function getContextPath() {
                return "${request.contextPath}";
            }
            var contextPath = getContextPath();
            var app = new Vue({
                el: '#app',
                data: {
                    result: []
                }
            });
            layui.use('upload',
            function() {
                var upload = layui.upload;
                var uploadInst = upload.render({
                    elem: '#importHostBtn',
                    url: '/host/importTemplate',
                    method: 'POST',
                    accept: 'file',
                    acceptMime: '.xlsx',
                    size: 64,
                    before: function(obj) {
                        layer.load();
                    },
                    done: function(res) {
                        layer.closeAll('loading');
                        getHostPageList();
                        layer.alert(res.msg);
                    },
                    error: function() {
                        layer.closeAll('loading');
                        getHostPageList();
                        layer.msg('网络异常');
                    }
                });
            });

            var getHostPageList = function(curr) {
                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: contextPath + "/host/getcond",
                    data: {
                        pageNum: curr || 1,
                        pageSize: 32,
                        name: $("#name").val(),
                        addr: $("#addr").val(),
                        remark: $("#remark").val()
                    },
                    success: function(msg) {
                        app.result = msg.result;
                        laypage({
                            cont: 'pagenav',
                            pages: msg.totalPage,
                            first: true,
                            last: true,
                            curr: curr || 1,
                            jump: function(obj, first) {
                                if (!first) {
                                    getHostPageList(obj.curr);
                                }
                            }
                        });
                    }
                });
            }
            getHostPageList();
            $('#findHost').on('click',
            function() {
                getHostPageList();
            });

            $('#addHostBtn').on('click',
            function() {
                layer.open({
                    type: 2,
                    title: '添加主机',
                    fix: false,
                    maxmin: true,
                    shadeClose: true,
                    area: ['1100px', '600px'],
                    content: contextPath + '/host/host_add',
                    end: function() {
                        getHostPageList();
                    }
                });

            });

            var showConsole = function(addr, port) {
                var url = 'http://' + addr + ":" + port + "/report";
                var tempWin = window.open(url, "_blank");
                $.ajax({
                    url: url,
                    type: 'GET',
                    cache: false,
                    dataType: "jsonp",
                    processData: false,
                    timeout: 1750,
                    complete: function(response) {
                        if (response.status == 200) {
                            tempWin.location.href = url;
                        } else {
                            tempWin.close();
                            layer.alert("访问失败");
                        }
                    }
                });
            }

            var editEvent = function(id) {
                layer.open({
                    type: 2,
                    title: '编辑主机',
                    fix: false,
                    maxmin: true,
                    shadeClose: true,
                    area: ['1100px', '600px'],
                    content: contextPath + '/host/editpage?id=' + id,
                    end: function() {
                        getHostPageList();
                    }
                });
            }

            var copyAddEvent = function(id) {
                layer.open({
                    type: 2,
                    title: '复制新增主机',
                    fix: false,
                    maxmin: true,
                    shadeClose: true,
                    area: ['1100px', '600px'],
                    content: contextPath + '/host/copyaddpage?id=' + id,
                    end: function() {
                        getHostPageList();
                    }
                });
            }

            var getReport = function(id) {
                var tempWin = window.open("about:blank"); // window.open("about:blank"); 
                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: contextPath + "/easynmon/getreport",
                    data: {
                        id: id
                    },
                    success: function(msg) {
                        var mes = msg.message;
                        if (mes.indexOf('http') < 0) {
                            layer.alert(mes);
                        } else {
                            tempWin.location.href = mes;

                        }
                    }
                });

            }

            var downloadTemplate = function() {
                var tempWin = window.open("about:blank");
                tempWin.location.href = contextPath + "/host/downloadTemplate";
            }

            var delEvent = function(id) {
                layer.msg('正在执行删除主机操作，请稍候...', {
                    icon: 16,
                    time: 1000
                });
                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: contextPath + "/host/del",
                    data: {
                        id: id
                    },
                    success: function(msg) {
                        getHostPageList();
                        layer.msg('删除成功', {
                            icon: 1,
                            time: 1000
                        });
                    }
                });
            }
            function setUpServer() {
                layer.msg('正在准备安装服务，这可能需要几秒钟时间...', {
                    icon: 16,
                    time: 7500
                });
                goEasyNmon(contextPath + '/easynmon/setupandrun');
            }

            function clearReport() {
                layer.confirm('将要清空报告？', {
                    btn: ['确定', '取消']
                },
                function() {
                    layer.msg('正在清空报告，请稍候...', {
                        icon: 16,
                        time: 1750
                    });
                    goEasyNmon(contextPath + '/easynmon/clearreport');
                },
                function() {
                });
            }
            function clearservice() {
                layer.confirm('将要卸载服务？', {
                    btn: ['确定', '取消']
                },
                function() {
                    layer.msg('正在卸载服务，请稍候...', {
                        icon: 16,
                        time: 1750
                    });
                    goEasyNmon(contextPath + '/easynmon/clearservice');
                },
                function() {
                });
            }
            function chgAll(t) {
                var ids = $.makeArray($("input[name='id']"));
                for (var i in ids) {
                    ids[i].checked = t.checked;
                }
            }
            function chg(t) {
                document.getElementById("ids").checked = true;
                var ids = $.makeArray($("input[name='id']"));
                for (var i in ids) {
                    if (ids[i].checked == false) {
                        document.getElementById("ids").checked = false;
                        return;
                    }

                }
            }

            function startEasyNmon() {
                var ids = $.makeArray($("input[name='id']"));
                var url = contextPath + '/easynmon/startmoniter';
                var flag = true;
                var tv = document.getElementById("t").value;
                var fv = document.getElementById("f").value;
                if (fv == '' || tv == '') {
                    layer.alert('参数不能为空');
                    return;
                }
                layer.msg('正在执行...', {
                    icon: 16,
                    time: 1250
                });
                url += "?t=" + tv + "&f=" + fv;
                for (var i in ids) {
                    if (ids[i].checked == true) {
                        flag = false;
                        ajaxurl(url + "&id=" + ids[i].value);
                    }
                }
                if (flag) {
                    layer.alert("未选择任何主机");
                    return;
                }
            }
            function stopEasyNmon() {
                layer.msg('正在执行...', {
                    icon: 16,
                    time: 1250
                });
                goEasyNmon(contextPath + '/easynmon/stopmoniter');
            }

            function goEasyNmon(url) {
                var ids = $.makeArray($("input[name='id']"));
                var flag = true;
                for (var i in ids) {
                    if (ids[i].checked == true) {
                        ajaxurl(url + "?id=" + ids[i].value);
                        flag = false;
                    }
                }
                if (flag) {
                    layer.alert("未选择任何主机");
                    return;
                }
            }

            var ajaxurl = function(url) {
                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: url,
                    data: {},
                    success: function(msg) {
                        layer.alert(msg.message);
                        layer.zIndex = layer.zIndex + 1;
                    }
                });
            }
            function getEasyNmonState() {
                var ids = $.makeArray($("input[name='id']"));
                for (var i in ids) {
                    var url = contextPath + "/easynmon/geteasynmonstate?id=" + ids[i].value;
                    $.ajax({
                        type: "GET",
                        dataType: "json",
                        url: url,
                        data: {},
                        success: function(msg) {
                            let pair = msg.message.split("_");
                            let id = pair[0];
                            let code = pair[1];
                            name = pair[2];
                            if (code == 200) {
                                document.getElementById('stat_' + id).innerHTML = '<span class="label label-success">运行正常</span>';
                            } else {
                                document.getElementById('stat_' + id).innerHTML = '<span class="label label-warning">无法访问</span>';
                            }
                        }
                    });
                }
            }
            $(document).ready(function(){setTimeout(function(){getEasyNmonState()},150);setInterval(function(){getEasyNmonState()},3000)});</script>
    </body>

</html>