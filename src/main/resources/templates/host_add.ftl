﻿<!DOCTYPE html>
<html lang="zh">
    
    <head>
        <meta charset="UTF-8">
        <title>添加主机</title>
        <link rel="stylesheet" href="${request.contextPath}/css/bootstrap.min.css">
        <script src="${request.contextPath}/js/jquery/jquery.min.js"></script>
        <script src="${request.contextPath}/js/jquery.validation/1.14.0/jquery.validate.min.js"></script>
        <script src="${request.contextPath}/js/jquery.validation/1.14.0/messages_zh.min.js"></script>
        <style>input.error{border: 1px solid #e6594e;}</style></head>
    
    <body>
        <div class="container">
            <form id="addHostForm">
                <br/>
                <div class="form-group">
                    <label for="hostname">主机名称：</label>
                    <input type="text" class="form-control" id="hostname" name="hostname" placeholder="设置主机名称"></div>
                <div class="form-group">
                    <label for="hostaddr">IP：</label>
                    <input type="text" class="form-control" id="hostaddr" name="hostaddr" placeholder="主机IP"></div>
                <div class="form-group">
                    <label for="hostaddr">SSH端口：</label>
                    <input type="text" class="form-control" id="hostport" name="hostport" value="22" placeholder="SSH端口"></div>
                <div class="form-group">
                    <label for="hostusername">用户名：</label>
                    <input type="text" class="form-control" id="hostusername" name="hostusername" value="root" placeholder="SSH用户名"></div>
                <div class="form-group">
                    <label for="hostpassword">密钥文件：（上传密钥后密码将不生效）</label>
                    <input type="file" class="form-control" id="hostkeyfile" name="hostpkeyfile" placeholder="密钥文件">
                    <input type="button" value="上传" onclick="submit2();" />
                    <input type="input" class="form-control" id="hostkeyfilepath" name="hostpkeyfilepath" readonly="readonly"></div>
                <div class="form-group">
                    <label for="hostpassword">密码：</label>
                    <input type="password" class="form-control" id="hostpassword" name="hostpassword" placeholder="SSH密码（若密钥包含密码请填写在此处）"></div>
                <div class="form-group">
                    <label for="eznmonport">设置easyNmon监控端口：</label>
                    <input type="text" class="form-control" id="eznmonport" name="eznmonport" value="9999" placeholder="可以任意设置，若非必要默认即可：9999"></div>
                <div class="form-group">
                    <label>系统版本：（默认nmon支持CentOS 6-8，Ubuntu、SUSE以及其他操作系统请手动指定）</label>
                    <select id="ostype" name="ostype" class="form-control">
                        <option value="">未选择</option></select>
                </div>
                <div class="form-group">
                    <label for="hosteremark">备注：</label>
                    <input type="text" class="form-control" id="hostremark" name="hostremark" placeholder="输入主机备注信息"></div>
                <div class="form-group">
                    <button type="button" id="saveBtn" class="btn btn-success">确定</button>
                    <button type="button" id="cancelBtn" class="btn btn-default">取消</button></div>
            </form>
        </div>
        <script>function submit2() {
                var type = "file";
                var id = "hostkeyfile";
                var formData = new FormData();
                formData.append(type, $("#" + id)[0].files[0]);
                $.ajax({
                    type: "POST",
                    url: '/upload',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        var msg = data.split("|")
                        //alert(msg);
                        alert(msg[0]);
                        if (msg.length > 1) {
                            $("#hostkeyfilepath").val(msg[1]);
                        }
                    }
                });
            }
            function getContextPath() {
                return "${request.contextPath}";
            }
            var contextPath = getContextPath();
            var addHost = function() {
                if (!check().form()) {
                    return;
                }
                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: contextPath + "/host/add",
                    data: {
                        "name": $("#hostname").val(),
                        "addr": $("#hostaddr").val(),
                        "port": $("#hostport").val(),
                        "username": $("#hostusername").val(),
                        "password": $("#hostpassword").val(),
                        "remark": $("#hostremark").val(),
                        "eznmonport": $("#eznmonport").val(),
                        "ostype": $("#ostype").val(),
                        "keyfilepath": $("#hostkeyfilepath").val()
                    },
                    success: function(msg) {
                        $('#cancelBtn').click();
                    }
                });
            }

            $('#saveBtn').on('click',
            function() {
                checkip();
            });

            $('#cancelBtn').on('click',
            function() {
                var index = parent.layer.getFrameIndex(window.name);
                parent.getHostPageList();
                parent.layer.close(index);
            });

            function check() {
                return $("#addHostForm").validate({
                    rules: {
                        hostname: {
                            required: true
                        },
                        hostaddr: {
                            required: true
                        },
                        hostport: {
                            required: true
                        },
                        hostusername: {
                            required: true
                        }
                    },
                    messages: {
                        hostname: {
                            required: ""
                        },
                        hostaddr: {
                            required: ""
                        },
                        hostport: {
                            required: ""
                        },
                        hostusername: {
                            required: ""
                        }
                    }
                });
            }
            function checkip() {
                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: contextPath + "/validate/checkip",
                    data: {
                        "name": $("#hostname").val(),
                        "addr": $("#hostaddr").val(),
                        "port": $("#hostport").val(),
                        "username": $("#hostusername").val(),
                        "password": $("#hostpassword").val(),
                        "remark": $("#hostremark").val(),
                        "ostype": $("#ostype").val(),
                        "keyfilepath": $("#hostkeyfilepath").val()
                    },
                    success: function(msg) {
                        if (msg.message == 'false') {
                            v_chkip = false;
                            alert('主机已经存在');

                        } else {
                            addHost();
                        }
                    }
                });
            }
            function getOSList() {
                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: contextPath + "/easynmon/getostype",
                    data: {},
                    success: function(msg) {
                        $('#ostype').html('');
                        var options = '';
                        options += '<option value="">未选择</option>';
                        var pair = msg.message.split(",");
                        for (var i = 0; i < pair.length; i++) {
                            options += '<option value="' + pair[i] + '">' + pair[i] + '</option>';
                        }
                        $('#ostype').append(options);
                    }
                });
            }
            getOSList();</script>
    </body>

</html>